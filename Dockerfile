FROM nginx:1.23-alpine

ARG USER=static
ARG WORKDIR=/usr/share/nginx/html

RUN set -aeux; \
    apk add --update curl; \
    adduser \
        --home ${WORKDIR} \
        --disabled-password \
        ${USER} ; \
    rm ${WORKDIR}/*

COPY --chown=${USER}:${USER} src/ ${WORKDIR}

HEALTHCHECK --interval=10s CMD curl --fail localhost:80/status || exit 1
